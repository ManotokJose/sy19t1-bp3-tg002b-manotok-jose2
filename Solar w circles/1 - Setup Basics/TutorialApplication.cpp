//---------------------------------------------------------------------------
/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)

	starSun = starSun->createPlanet(*mSceneMgr, 20, ColourValue(1, 1, 0), "materialSun");
	starSun->setLocalRotationSpeed(1);
	starSun->setRevolutionSpeed(420);

	//merc
	planetMercury = planetMercury->createPlanet(*mSceneMgr, 3, ColourValue(0.82f, 0.7f, 0.54f), "materialMerc");
	planetMercury->setParent(starSun);
	planetMercury->setRevolutionSpeed(88);
	planetMercury->setLocalRotationSpeed(1416);

	//ven
	planetVenus = planetVenus->createPlanet(*mSceneMgr, 5, ColourValue(0.93f, 0.9f, 0.67f), "materialVen");
	planetVenus->setParent(starSun);
	planetVenus->setRevolutionSpeed(227.4);
	planetVenus->setLocalRotationSpeed(5832);

	//earth values
	planetEarth = planetEarth->createPlanet(*mSceneMgr, 10, ColourValue::Blue, "materialEarth");
	planetEarth->setParent(starSun);
	planetEarth->setRevolutionSpeed(365.2);
	planetEarth->setLocalRotationSpeed(0.98166667);

	//moon values
	earthMoon = earthMoon->createPlanet(*mSceneMgr, 1, ColourValue(0.7f, 0.7f, 0.7f), "materialMoon");
	earthMoon->setParent(planetEarth);
	earthMoon->setRevolutionSpeed(12);
	earthMoon->setLocalRotationSpeed(12);

	//mars
	planetMars = planetMars->createPlanet(*mSceneMgr, 8, ColourValue(0.71f, 0.25f, 0.05f), "materialMars");
	planetMars->setParent(starSun);
	planetMars->setRevolutionSpeed(687);
	planetMars->setLocalRotationSpeed(1.0154167);

	planetMercury->getNode().translate(36, 0, 0);
	planetVenus->getNode().translate(67.24, 0, 0);
	earthMoon->getNode().translate(82.9, 0, 0);
	planetEarth->getNode().translate(92.9, 0, 0);
	planetMars->getNode().translate(141.71, 0, 0);

	solarSystem.push_back(starSun);
	solarSystem.push_back(planetMercury);
	solarSystem.push_back(planetVenus);
	solarSystem.push_back(planetEarth);
	solarSystem.push_back(earthMoon);
	solarSystem.push_back(planetMars);
	
	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	// Values taken from: http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation

	// Lecture: Setting ambient light
	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));
}
bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	for (int i = 0; i < 6; i++)
	{
		solarSystem[i]->update(evt);
	}
	return true;
}
//---------------------------------------------------------------------------

//doesnt work for some reason bye bye quality :(
//void TutorialApplication::createScene(void)
//{
//	// Create your scene here :)
//	setPlanetValues(starSun, NULL, 20, ColourValue(1, 1, 0), 100, 0, 0);
//	setPlanetValues(planetMercury, starSun, 3, ColourValue(0.82f, 0.7f, 0.54f), 88, 1416, 36);
//	setPlanetValues(planetVenus, starSun, 5, ColourValue(0.93f, 0.9f, 0.67f), 227.4, 5832, 67.24);
//	setPlanetValues(planetEarth, starSun, 10, ColourValue::Blue, 365.2, 0.98166667, 92.9);
//	setPlanetValues(earthMoon, starSun, 1, ColourValue(0.7f, 0.7f, 0.7f), 12, 12, 82.9);
//	setPlanetValues(planetMars, starSun, 8, ColourValue(0.71f, 0.25f, 0.05f), 687, 1.0154167, 141.71);
//
//	solarSystem.push_back(starSun);
//	solarSystem.push_back(planetMercury);
//	solarSystem.push_back(planetVenus);
//	solarSystem.push_back(planetEarth);
//	solarSystem.push_back(earthMoon);
//	solarSystem.push_back(planetMars);
//}
//bool TutorialApplication::frameStarted(const FrameEvent& evt)
//{
//	for (int i = 0; i < 6; i++)
//	{
//		solarSystem[i]->update(evt);
//	}
//	return true;
//}
//void TutorialApplication::setPlanetValues(Planet* planet, Planet* parent, float size, ColourValue colour, float rotateSpeed, float revSpeed, float posX)
//{
//	planet = planet->createPlanet(*mSceneMgr, size, colour);
//	planet->setParent(parent);
//	planet->setRevolutionSpeed(revSpeed);
//	planet->setLocalRotationSpeed(rotateSpeed);
//	planet->getNode().translate(posX, 0, 0);
//}
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
