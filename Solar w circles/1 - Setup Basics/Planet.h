#pragma once
#include <OgreManualObject.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <vector>

using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* node);
	static Planet* createPlanet(SceneManager& sceneManager, float size, ColourValue colour, std::string materialName);
	~Planet();

	void update(const FrameEvent& evt);

	SceneNode& getNode();
	void setParent(Planet* parent);
	Planet* getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

	static void createStack(ManualObject* object, int vectorPoint, float radius, float radius2, float angle, ColourValue colour);

private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mLocalRevolutionSpeed;

	//std::vector<ManualObject*> planetObjects;
};

