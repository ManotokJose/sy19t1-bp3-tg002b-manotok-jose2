#include "Planet.h"

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

//site that helped http://wiki.ogre3d.org/ManualSphereMeshes

Planet* Planet::createPlanet(SceneManager& sceneManager, float size, ColourValue colour, std::string materialName)
{
	SceneNode* planetNode = sceneManager.getRootSceneNode()->createChildSceneNode();
	ManualObject* object = sceneManager.createManualObject();

	MaterialPtr objectMaterial = Ogre::MaterialManager::getSingleton().create(materialName, "General");
	objectMaterial->setReceiveShadows(true);

	if (materialName == "materialSun")
	{
		objectMaterial->getTechnique(0)->setLightingEnabled(false);
	}
	else
	{
		objectMaterial->getTechnique(0)->setLightingEnabled(true);
	}
	
	objectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);

	object->begin(materialName, RenderOperation::OT_TRIANGLE_LIST);

	//number of times the angle is split, tells if shpere is high poly or low poly
	//better shape = higher number
	int numberOfAngles = 16;

	float sinAngle = (3.14 / numberOfAngles);
	float cosAngle = (2 * 3.14 / numberOfAngles);
	int indexNumber = 0;

	//i = stacks
	//Y is 5 if i is 0
	//Y is -4.99 is i = 8

	//limit i to be <= number of angles to make circle
	for (int i = 0; i <= numberOfAngles; i++)
	{
		float radius = size / 2 * sinf(i * sinAngle);	
		float stackY = size / 2 * cosf(i * sinAngle);

		for (int vectorPoint = 0; vectorPoint <= numberOfAngles; vectorPoint++)
		{
			createStack(object, vectorPoint, radius, stackY, cosAngle, colour);

			if (i != numberOfAngles)
			{
				//makes box
				object->index(indexNumber + numberOfAngles + 1);
				object->index(indexNumber);
				object->index(indexNumber + numberOfAngles);
				object->index(indexNumber + numberOfAngles + 1);
				object->index(indexNumber + 1);
				object->index(indexNumber);
				
				//so it wont call the same index
				indexNumber++;
			};
		};
	};

	object->end();

	planetNode->attachObject(object);

	return new Planet(planetNode);
}

Planet::~Planet()
{
}

void Planet::update(const FrameEvent& evt)
{
	Degree planetRotation = Degree(6 * ((24/mLocalRotationSpeed) * evt.timeSinceLastFrame));
	Degree planetRevolution = Degree(6 * ((365/mLocalRevolutionSpeed) * evt.timeSinceLastFrame));
	if (mParent != NULL)
	{
		float oldX = mNode->getPosition().x - mParent->mNode->getPosition().x;
		float oldZ = mNode->getPosition().z - mParent->mNode->getPosition().z;
		float newX = (oldX * Math::Cos(planetRevolution)) + (oldZ * Math::Sin(planetRevolution)) + mParent->getNode().getPosition().x;
		float newZ = (oldX * -Math::Sin(planetRevolution)) + (oldZ * Math::Cos(planetRevolution)) + mParent->getNode().getPosition().z;
		mNode->setPosition(newX, mNode->getPosition().y, newZ);
	}

	mNode->rotate(Vector3(0, -1, 0), Radian(planetRotation));
}

SceneNode& Planet::getNode()
{
	// TODO: insert return statement here
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mLocalRevolutionSpeed = speed;
}

void Planet::createStack(ManualObject* object, int vectorPoint, float radius, float posY, float angle, ColourValue colour)
{
	//make new point
	float posX = radius * sinf(vectorPoint * angle);
	float posZ = radius * cosf(vectorPoint * angle);

	object->position(posX, posY, posZ);
	object->colour(colour);
	object->normal(Vector3(posX, posY, posZ).normalisedCopy());
}
