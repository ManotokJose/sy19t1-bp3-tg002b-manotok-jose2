//---------------------------------------------------------------------------
/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)

	//sun values
	starSun = starSun->createPlanet(*mSceneMgr, 20, ColourValue::Red);
	starSun->setLocalRotationSpeed(1);
	starSun->setRevolutionSpeed(420);

	//merc
	planetMercury = planetMercury->createPlanet(*mSceneMgr, 3, ColourValue::Red);
	planetMercury->setParent(starSun);
	planetMercury->setRevolutionSpeed(88);
	planetMercury->setLocalRotationSpeed(1416);

	//ven
	planetVenus = planetVenus->createPlanet(*mSceneMgr, 5, ColourValue::Red);
	planetVenus->setParent(starSun);
	planetVenus->setRevolutionSpeed(227.4);
	planetVenus->setLocalRotationSpeed(5832);

	//earth values
	planetEarth = planetEarth->createPlanet(*mSceneMgr, 10, ColourValue::Blue);
	planetEarth->setParent(starSun);
	planetEarth->setRevolutionSpeed(365.2);
	planetEarth->setLocalRotationSpeed(0.98166667);

	//moon values
	earthMoon = earthMoon->createPlanet(*mSceneMgr, 1, ColourValue::White);
	earthMoon->setParent(planetEarth);
	earthMoon->setRevolutionSpeed(12);
	earthMoon->setLocalRotationSpeed(12);

	//mars
	planetMars = planetMars->createPlanet(*mSceneMgr, 8, ColourValue::Blue);
	planetMars->setParent(starSun);
	planetMars->setRevolutionSpeed(687);
	planetMars->setLocalRotationSpeed(1.0154167);


	//whoops
	//jupiter
	planetJupiter = planetJupiter->createPlanet(*mSceneMgr, 9, ColourValue::Blue);
	planetJupiter->setParent(starSun);
	planetJupiter->setRevolutionSpeed(4330.9);
	planetJupiter->setLocalRotationSpeed(0.3979167);

	//saturn
	planetSatrun = planetSatrun->createPlanet(*mSceneMgr, 8, ColourValue::Blue);
	planetSatrun->setParent(starSun);
	planetSatrun->setRevolutionSpeed(10752.9);
	planetSatrun->setLocalRotationSpeed(0.4333333);

	//anus
	planetAnus = planetAnus->createPlanet(*mSceneMgr, 7, ColourValue::Blue);
	planetAnus->setParent(starSun);
	planetAnus->setRevolutionSpeed(30660);
	planetAnus->setLocalRotationSpeed(0.7);

	//neptune
	planetNeptune = planetNeptune->createPlanet(*mSceneMgr, 7, ColourValue::Blue);
	planetNeptune->setParent(starSun);
	planetNeptune->setRevolutionSpeed(60225);
	planetNeptune->setLocalRotationSpeed(0.3979167);

	//pluto
	planetPluto = planetPluto->createPlanet(*mSceneMgr, 1, ColourValue::Blue);
	planetPluto->setParent(starSun);
	planetPluto->setRevolutionSpeed(90520);
	planetPluto->setLocalRotationSpeed(6.9); //hehe

	planetMercury->getNode().translate(36, 0, 0);
	planetVenus->getNode().translate(67.24, 0, 0);
	earthMoon->getNode().translate(82.9, 0, 0);
	planetEarth->getNode().translate(92.9, 0, 0);
	planetMars->getNode().translate(141.71, 0, 0);
	planetJupiter->getNode().translate(483.88, 0, 0);
	planetSatrun->getNode().translate(887.14, 0, 0);
	planetAnus->getNode().translate(1783.98, 0, 0);
	planetNeptune->getNode().translate(2796.46, 0, 0);
	planetPluto->getNode().translate(3666, 0, 0);
}
bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	starSun->update(evt);
	planetMercury->update(evt);
	planetVenus->update(evt);
	planetEarth->update(evt);	 
	earthMoon->update(evt);
	planetMars->update(evt);
	planetJupiter->update(evt);
	planetSatrun->update(evt);
	planetAnus->update(evt);
	planetNeptune->update(evt);
	planetPluto->update(evt);
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
