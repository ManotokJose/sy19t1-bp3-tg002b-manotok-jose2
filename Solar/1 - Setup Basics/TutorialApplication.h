/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"
#include "Planet.h"

using namespace Ogre;

//---------------------------------------------------------------------------

class TutorialApplication : public BaseApplication
{
public:
    TutorialApplication(void);
    virtual ~TutorialApplication(void);

protected:
    virtual void createScene(void);
	bool frameStarted(const FrameEvent& evt);

private:
	Planet* starSun;
	Planet* planetMercury;
	Planet* planetVenus;
	Planet* planetEarth;
	Planet* earthMoon;
	Planet* planetMars;
	Planet* planetJupiter;
	Planet* planetSatrun;
	Planet* planetAnus;
	Planet* planetNeptune;
	Planet* planetPluto;

	//need to code*
	//jupiter and all its glorious 70+ moons
	//satrun and its amazing 80+ moons
	//the asteriod field in all its greatness
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
