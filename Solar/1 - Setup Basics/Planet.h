#pragma once
#include <OgreManualObject.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>

using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* node);
	static Planet* createPlanet(SceneManager& sceneManager, float size, ColourValue colour);
	~Planet();

	void update(const FrameEvent& evt);
	//since the moons orbit isnt a perfect circle (a lot of the planets orbit isnt a perfect circle but SHHHHHHHHHHHHHHHHH)

	SceneNode& getNode();
	void setParent(Planet* parent);
	Planet* getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);
private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mLocalRevolutionSpeed;
};

