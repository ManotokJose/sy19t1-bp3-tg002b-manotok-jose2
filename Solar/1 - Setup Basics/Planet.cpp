#include "Planet.h"

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet* Planet::createPlanet(SceneManager& sceneManager, float size, ColourValue colour)
{
	SceneNode* planetNode = sceneManager.getRootSceneNode()->createChildSceneNode();
	ManualObject* object = sceneManager.createManualObject();
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//creating the object
	//s1
	object->position(-size, size, size);
	object->position(-size, -size, size);
	object->position(size, -size, size);

	object->position(-size, size, size);
	object->position(size, -size, size);
	object->position(size, size, size);

	//s2	
	object->position(size, size, size);
	object->position(size, -size, size);
	object->position(size, -size, -size);

	object->position(size, size, size);
	object->position(size, -size, -size);
	object->position(size, size, -size);

	//s3
	object->position(-size, size, size);
	object->position(-size, -size, -size);
	object->position(-size, -size, size);

	object->position(-size, size, size);
	object->position(-size, size, -size);
	object->position(-size, -size, -size);

	//s4
	object->position(size, size, -size);
	object->position(size, -size, -size);
	object->position(-size, -size, -size);

	object->position(size, size, -size);
	object->position(-size, -size, -size);
	object->position(-size, size, -size);

	//s5
	object->position(-size, size, -size);
	object->position(-size, size, size);
	object->position(size, size, size);

	object->position(-size, size, -size);
	object->position(size, size, size);
	object->position(size, size, -size);


	//s6
	object->position(-size, -size, size);
	object->position(-size, -size, -size);
	object->position(size, -size, -size);

	object->position(-size, -size, size);
	object->position(size, -size, -size);
	object->position(size, -size, size);


	object->end();

	planetNode->attachObject(object);

	return new Planet(planetNode);
}

Planet::~Planet()
{
}

void Planet::update(const FrameEvent& evt)
{
	Degree planetRotation = Degree(6 * ((24/mLocalRotationSpeed) * evt.timeSinceLastFrame));
	Degree planetRevolution = Degree(6 * ((365/mLocalRevolutionSpeed) * evt.timeSinceLastFrame));
	if (mParent != NULL)
	{
		float oldX = mNode->getPosition().x - mParent->mNode->getPosition().x;
		float oldZ = mNode->getPosition().z - mParent->mNode->getPosition().z;
		float newX = (oldX * Math::Cos(planetRevolution)) + (oldZ * Math::Sin(planetRevolution)) + mParent->getNode().getPosition().x;
		float newZ = (oldX * -Math::Sin(planetRevolution)) + (oldZ * Math::Cos(planetRevolution)) + mParent->getNode().getPosition().z;
		mNode->setPosition(newX, mNode->getPosition().y, newZ);
	}

	mNode->rotate(Vector3(0, -1, 0), Radian(planetRotation));
}

SceneNode& Planet::getNode()
{
	// TODO: insert return statement here
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mLocalRevolutionSpeed = speed;
}
