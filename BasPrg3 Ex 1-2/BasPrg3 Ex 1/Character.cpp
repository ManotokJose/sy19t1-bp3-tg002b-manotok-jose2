#include "Character.h"



Character::Character()
{
	health = 100;
	mp = 100;
	maxMp = 100;
	characterWeapon = new Weapon();
	characterSkill = new Skill();
	characterSkill->skillWeapon = characterWeapon;

}

Character::~Character()
{
	delete characterWeapon;
	delete characterSkill;
}

int Character::getDamage()
{
	int totalDamage = 0;
	mp += characterManaRegen();
	for (int i = 0; i < 1; i++)
	{
		skillNumber = randomNumber();
		switch (skillNumber)
		{
		case 0:
			totalDamage += characterSkill->getTotalDamage(false);
			break;

		case 1:
			totalDamage += characterSkill->getTotalDamage(true);
			break;

		case 2:
			health -= skillVengance->getSelfDamage();
			totalDamage += skillVengance->getVenganceDamage();
			break;

		case 3:
			health += skillSyphon->getLifeSteal();
			totalDamage += skillSyphon->getTotalDamage(false);
			break;

		case 4:
			i--;
			totalDamage += skillSyphon->getTotalDamage(false);
			break;
			
		default:
			break;
		}
		i++;
	}
	return totalDamage;
}

int Character::getHealth()
{
	return health;
}

void Character::attackCharacter(Character* enemy)
{
	enemy->health -= this->getDamage();
}

//not implemented
bool Character::mpCheck(int cmp)
{
	if (cmp <= mp)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//

int Character::randomNumber()
{
	return rand() % 2 + 0;
}

//not implemented
int Character::characterManaRegen()
{
	return maxMp * .25;
}
//
