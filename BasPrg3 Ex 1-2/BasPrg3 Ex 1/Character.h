#pragma once
#include "Skill.h"
#include "skillVengance.h"
#include "skillSyphon.h"
#include <string>
#include <iostream>

using namespace std;

class Character
{
public:

	void attackCharacter(Character* enemy);
	bool mpCheck(int cmp);
	int randomNumber();
	int characterManaRegen();
	int getDamage();
	int getHealth();
	Character();
	~Character();

private:

	int health;
	int mp;
	int maxMp;
	int skillNumber;
	Skill* characterSkill;
	skillVengance* skillVengance;
	skillSyphon* skillSyphon;
	Weapon* characterWeapon;

};

