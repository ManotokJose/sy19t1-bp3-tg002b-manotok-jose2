#include <string>
#include <iostream>
#include <time.h>
#include "Character.h"

using namespace std;

int main()
{
	srand(time(NULL));

	Character* player = new Character;
	Character* enemy = new Character;

	while (player->getHealth() > 0 && enemy->getHealth() > 0)
	{
		cout << "Player health: " << player->getHealth() << endl;
		cout << "Enemy health: " << enemy->getHealth() << endl;
		
		cout << "Player attacks the enemy!" << endl;
		player->attackCharacter(enemy);
		
		system("pause");

		cout << "Enemy attacks the player!" << endl;
		enemy->attackCharacter(player);

		system("pause");
	}

	if (player->getHealth() > 0)
	{
		cout << "Player wins with " << player->getHealth() << " HP left!";
	}
	else
	{
		cout << "Enemy wins with " << enemy->getHealth() << " HP left!";
	}
	cout << endl;
	system("pause");

	delete player;
	delete enemy;
}