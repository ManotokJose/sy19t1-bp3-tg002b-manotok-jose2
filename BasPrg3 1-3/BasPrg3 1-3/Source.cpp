#include <string>
#include <iostream>
#include <time.h>
#include <vector>
#include <time.h>
#include "Character.h"
#include "Skill.h"
#include "CritSkill.h"
#include "SyphonSkill.h"
#include "VenganceSkill.h"
#include "DopelSkill.h"

using namespace std;

int getRandomNumber(int skillSize);

int main()
{
	srand(time(NULL));
	
	string name;
	cin >> name;
	Character* player = new Character(name);
	cin >> name;
	Character* enemy = new Character(name);

	vector<Skill*> characterSkills;
	characterSkills.push_back(new Skill);
	characterSkills.push_back(new CritSkill);
	characterSkills.push_back(new SyphonSkill);
	characterSkills.push_back(new VenganceSkill);
	characterSkills.push_back(new DopelSkill);

	while (player->getHealth() > 0 && enemy->getHealth() > 0)
	{
		system("cls");
		player->displayStats();
		cout << endl;
		cout << "===========================================================================" << endl;
		cout << endl;
		enemy->displayStats();
		system("pause");
		characterSkills[getRandomNumber(characterSkills.size())]->doSkillAction(player, enemy);
		characterSkills[getRandomNumber(characterSkills.size())]->doSkillAction(enemy, player);
		player->addMana(5);
		enemy->addMana(5);
		system("pause");
	}

	for (int i = 0; i < characterSkills.size(); i++)
	{
		delete characterSkills[i];
	}

	delete player;
	delete enemy;
	return 0;
}

int getRandomNumber(int skillSize)
{
	return rand() % skillSize + 0;
}
