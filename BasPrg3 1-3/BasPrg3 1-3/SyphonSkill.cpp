#include "SyphonSkill.h"



SyphonSkill::SyphonSkill()
{
}


SyphonSkill::~SyphonSkill()
{
}

void SyphonSkill::doSkillAction(Character* player, Character* enemy)
{
	if (player->getMana() < 35)
	{
		cout << player->getName() << ": " << "Syphon Attack" << endl;
		cout << "Not enough mana" << endl;
		enemy->addHealth(-1 * player->getCharacterDamage());
	}
	else
	{
		cout << player->getName() << ": " << "Syphon Attack" << endl;
		player->addHealth(.25 * player->getCharacterDamage());
		enemy->addHealth(-1 * player->getCharacterDamage());
		player->addMana(-35);

	}
}
