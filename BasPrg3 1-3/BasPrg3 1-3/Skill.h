#pragma once
#include <string>
#include <iostream>
#include "Character.h"

using namespace std;

class Skill
{
public:
	Skill();
	~Skill();

	virtual void doSkillAction(Character* player, Character* enemy);
	
};

