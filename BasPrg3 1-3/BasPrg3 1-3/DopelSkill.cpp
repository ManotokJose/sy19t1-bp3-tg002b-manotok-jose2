#include "DopelSkill.h"




DopelSkill::DopelSkill()
{
}


DopelSkill::~DopelSkill()
{
}

void DopelSkill::doSkillAction(Character* player, Character* enemy)
{
	if (player->getMana() < 75)
	{
		cout << player->getName() << ": " << "DopelBlade" << endl;
		cout << "Not enough mana" << endl;
		enemy->addHealth(-1 * player->getCharacterDamage());
	}
	else
	{
		int randomNumber = rand() % 3 + 0;
		Character* playerClone = new Character("Clone");
		cout << "DopelBlade" << endl;
		cout << player->getName() << " summons a clone" << endl;

		vector<Skill*> cloneSkills;
		cloneSkills.push_back(new Skill);

		cloneSkills.push_back(new CritSkill);
		cloneSkills.push_back(new SyphonSkill);
		cloneSkills.push_back(new VenganceSkill);

		cout << "Normal Attack" << endl;

		enemy->addHealth(-1 * player->getCharacterDamage());
		cloneSkills[randomNumber]->doSkillAction(playerClone, enemy);
		player->addMana(-75);
		delete playerClone;
	}
}
