#pragma once
#include <string>
#include <iostream>
#include "Character.h"
#include "Skill.h"

using namespace std;

class CritSkill : public Skill
{
public:
	CritSkill();
	~CritSkill();

	void doSkillAction(Character* player, Character* enemy);
};

