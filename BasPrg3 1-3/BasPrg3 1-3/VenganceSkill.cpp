#include "VenganceSkill.h"



VenganceSkill::VenganceSkill()
{
}


VenganceSkill::~VenganceSkill()
{
}

void VenganceSkill::doSkillAction(Character* player, Character* enemy)
{
	if (player->getMana() < 60)
	{
		cout << player->getName() << ": " << "Vengance Attack" << endl;
		cout << "Not enough mana" << endl;
		enemy->addHealth(-1 * player->getCharacterDamage());
	}
	else
	{
		cout << player->getName() << ": " << "Vengance Attack" << endl;
		enemy->addHealth(-1 * ((player->getCharacterDamage() * 2) + player->getCharacterDamage()));
		player->addMana(-60);
	}

	if (player->getHealth() > 25)
	{
		player->addHealth(-25);
	}
	else
	{
		player->setHealth(1);
	}
	
}
