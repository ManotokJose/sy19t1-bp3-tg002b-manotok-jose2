#include "Character.h"



Character::Character(string name)
{
	characterName = name;
	health = 100;
	mana = 100;
	characterWeapon = new Weapon();
}


Character::~Character()
{
	delete characterWeapon;
}

int Character::getCharacterDamage()
{
	return characterWeapon->getDamage();
}

int Character::getHealth()
{
	return health;
}

int Character::getMana()
{
	return mana;
}

string Character::getName()
{
	return characterName;
}

void Character::addHealth(int number)
{
	health += number;

	if (health > 100)
	{
		health = 100;
	}
}

void Character::setHealth(int number)
{
	health = number;
}

void Character::addMana(int number)
{
	mana += number;

	if (mana > 100)
	{
		mana = 100;
	}
}

void Character::displayStats()
{
	cout << "Name: " << characterName << endl;
	cout << "Health: " << health << endl;
	cout << "Mana: " << mana << endl;
}


