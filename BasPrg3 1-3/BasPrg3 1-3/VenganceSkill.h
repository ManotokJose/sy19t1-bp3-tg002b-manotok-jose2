#pragma once
#include <string>
#include <iostream>
#include "Character.h"
#include "Skill.h"

using namespace std;

class VenganceSkill : public Skill
{
public:
	VenganceSkill();
	~VenganceSkill();

	void doSkillAction(Character* player, Character* enemy);
};

