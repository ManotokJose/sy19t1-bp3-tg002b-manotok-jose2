#pragma once
#include "Weapon.h"
#include <iostream>

using namespace std;

class Character
{
public:
	Character(string name);
	~Character();
	Weapon* characterWeapon;
	int getCharacterDamage();
	int getHealth();
	int getMana();
	string getName();
	void addHealth(int number);
	void setHealth(int number);
	void addMana(int number);
	void displayStats();
private:
	string characterName;
	int health;
	int mana;
};

