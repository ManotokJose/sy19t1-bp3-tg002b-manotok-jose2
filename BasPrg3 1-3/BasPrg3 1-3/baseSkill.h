#pragma once
#include "Character.h"
class baseSkill
{
public:
	baseSkill();
	~baseSkill();

	void assignCharacter(Character* skillCharacter);
	virtual void doSkillAction(Character* enemy);
private:
	Character* player;
};

