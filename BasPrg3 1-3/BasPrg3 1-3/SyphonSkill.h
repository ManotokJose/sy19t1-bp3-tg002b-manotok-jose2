#pragma once
#include <string>
#include <iostream>
#include "Character.h"
#include "Skill.h"

using namespace std;

class SyphonSkill : public Skill
{
public:
	SyphonSkill();
	~SyphonSkill();

	void doSkillAction(Character* player, Character* enemy);
};

