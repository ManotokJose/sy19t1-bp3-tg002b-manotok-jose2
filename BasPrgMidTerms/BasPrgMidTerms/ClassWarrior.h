#pragma once
#include "Class.h"
#include <iostream>

using namespace std;

class ClassWarrior : public Class
{
public:
	ClassWarrior();
	~ClassWarrior();

	void setClassLevel();
	void attackEnemy(vector<Class*> enemy, bool isBasicAttack);
	bool hasBonusDamage(Class* enemy);
	int getClassLevel();
	void displaySkills();
};

