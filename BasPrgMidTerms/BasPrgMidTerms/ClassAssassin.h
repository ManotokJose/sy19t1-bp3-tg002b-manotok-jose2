#pragma once
#include "Class.h"
class ClassAssassin : public Class
{
public:
	ClassAssassin();
	~ClassAssassin();

	void setClassLevel();
	void attackEnemy(vector<Class*> enemy, bool isBasicAttack);
	bool hasBonusDamage(Class* enemy);
	int getClassLevel();
	void displaySkills();
};

