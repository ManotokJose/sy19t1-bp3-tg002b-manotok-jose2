#include "ClassWarrior.h"



ClassWarrior::ClassWarrior()
{
	className = "Enemy Warrior";

	classType = "Warrior";
	classLevel = 1;
	classPow = 10;
	classHealth = 20;
	classAgi = 1;
	classVit = 15;
}


ClassWarrior::~ClassWarrior()
{
}

void ClassWarrior::setClassLevel()
{
	classLevel++;
}

void ClassWarrior::attackEnemy(vector<Class*> enemy, bool isBasicAttack)
{
	int damage;
	if (isBasicAttack == false)
	{
		cout << className << " used shockwave" << endl;
		for (int i = 0; i < enemy.size(); i++)
		{
			damage = setDamage(enemy[i], hasBonusDamage(enemy[i]), 0.9);
			cout << damage << " was dealt to " << enemy[i]->getClassName();
		}
	}
	else
	{
		damageEnemy(enemy, 1.0);
	}
}

bool ClassWarrior::hasBonusDamage(Class* enemy)
{
	if (enemy->getClassType() == "Assassin")
	{
		return true;
	}
	else
	{
		return false;
	}
}

int ClassWarrior::getClassLevel()
{
	return classLevel;
}

void ClassWarrior::displaySkills()
{
	cout << "[1] Basic Attack" << endl;
	cout << "[2] Shockwave" << endl;
}
