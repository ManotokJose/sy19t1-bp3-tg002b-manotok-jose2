#include "Player.h"
#include <iostream>

Player::Player(bool randomMembers)
{
	if (randomMembers == false)
	{
		int playerInput = 1;
		for (int i = 0; i < 3; i++)
		{
			do
			{
				if (playerInput < 1 || playerInput > 3)
				{
					cout << "Wrong input" << endl;
				}
				cout << "Member #" << i + 1 << ": " << endl;
				cout << "[1] Assassin" << endl;
				cout << "[2] Mage" << endl;
				cout << "[3] Warrior" << endl;
				cin >> playerInput;

			} while (playerInput < 1 || playerInput > 3);

			switch (playerInput)
			{
			case 1:
				playerTeam.push_back(new ClassAssassin);
				break;
			case 2:
				playerTeam.push_back(new ClassMage);
				break;
			case 3:
				playerTeam.push_back(new ClassWarrior);
				break;
			}
			playerTeam[i]->setClassName();
			playerTeam[i]->setPlayerUnit(true);
			playerTeam[i]->setUnitNumber(i);
		}
	}
	else
	{
		for (int i = 0; i < 3; i++)
		{
			playerTeam.push_back(randomClass());
			playerTeam[i]->setPlayerUnit(false);
		}
	}
}


Player::~Player()
{
	for (int i = 0; i < playerTeam.size(); i++)
	{
		delete playerTeam[i];
	}
}

bool Player::isMemberDead(int member)
{
	if (playerTeam[member]->getHealth() <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Player::isTeamDead()
{
	if (playerTeam[0]->getHealth() <= 0 && playerTeam[1]->getHealth() <= 0 && playerTeam[2]->getHealth() <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

Class* Player::randomClass()
{
	int randomNumber = rand() % 2 + 0;
	switch (randomNumber)
	{
	case 0:
		return new ClassAssassin;
		break;

	case 1:
		return new ClassMage;
		break;

	case 2:
		return new ClassWarrior;
		break;

	}
}
