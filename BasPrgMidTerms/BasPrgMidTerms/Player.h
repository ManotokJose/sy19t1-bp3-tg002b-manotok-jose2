#pragma once
#include <vector>
#include "Class.h"
#include "ClassAssassin.h"
#include "ClassMage.h"
#include "ClassWarrior.h"

using namespace std;

class Player
{
public:
	Player(bool randomMembers);
	~Player();

	bool isMemberDead(int member);
	bool isTeamDead();

	Class* randomClass();
	vector<Class*>playerTeam;
private:
	
};

