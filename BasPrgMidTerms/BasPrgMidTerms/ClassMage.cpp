#include "ClassMage.h"



ClassMage::ClassMage()
{
	className = "Enemy Mage";

	classType = "Mage";
	classLevel = 1;
	classPow = 5;
	classHealth = 15;
	classAgi = 2;
	classVit = 10;
}


ClassMage::~ClassMage()
{
}

void ClassMage::setClassLevel()
{
	classLevel++;
}

void ClassMage::attackEnemy(vector<Class*> team, bool isBasicAttack)
{
	int damage;
	bool bonusDamage;
	if (isBasicAttack == false)
	{
		Class* target = team[0];
		for (int i = 0; i < team.size(); i++)
		{
			if (target->getHealth() > team[i]->getHealth())
			{
				target = team[i];
			}
		}

		delete target;
	}
	else
	{
		int randomNumber = rand() % enemy.size() + 0;
		cout << className << " used a basic attack against " << enemy[randomNumber]->getClassName();
		enemy[randomNumber]->setClassHealth(getBaseDamage(1.0) - enemy[randomNumber]->getClassVit());
	}
}

int ClassMage::getClassLevel()
{
	return classLevel;
}

