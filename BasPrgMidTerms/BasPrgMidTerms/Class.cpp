#include "Class.h"
#include <iostream>

using namespace std;

Class::Class()
{
}


Class::~Class()
{
}

int Class::getBaseDamage(float coEff)
{
	int maxRandomPower = (classPow * 1.2) - 1;
	int randomPower = rand() % maxRandomPower + classPow;
	if ((randomPower * coEff) <= 0)
	{
		return 1;
	}
	return (randomPower * coEff);
}

int Class::getClassLevel()
{
	return 0;
}

int Class::getHealth()
{
	return classHealth;
}

int Class::getAgi()
{
	return classAgi;
}

int Class::getClassDex()
{
	return classDex;
}

int Class::getClassVit()
{
	return classVit;
}

int Class::getUnitNumber()
{
	return unitNumber;
}

int Class::damageCrit(int number)
{
	int randomNumber = rand() % 100 + 1;
	if (randomNumber <= 20)
	{
		return number * 1.2;
	}
	return number;
}

bool Class::isPlayerUnit()
{
	return playerUnit;
}

string Class::getClassName()
{
	return className;
}

string Class::getClassType()
{
	return classType;
}

void Class::setClassName()
{
	string name;
	cout << "Name of this character: ";
	cin >> name;
	className = name;
}

void Class::setClassHealth(int number)
{
	classHealth += number;
}

void Class::setPlayerUnit(bool isPlayerUnit)
{
	playerUnit = isPlayerUnit;
}

void Class::setUnitNumber(int number)
{
	unitNumber = number;
}

int Class::setDamage(Class* enemy, bool bonusDamage, int coEff)
{
	if (hasBonusDamage(enemy))
	{
		return (getBaseDamage(coEff) - enemy->getClassVit()) * 1.5;
	}
	else
	{
		return (getBaseDamage(coEff) - enemy->getClassVit());
	}
}

void Class::damageEnemy(vector<Class*> enemy, float coEff)
{
	int damage;
	int randomNumber = rand() % enemy.size() + 0;
	int hitRateChance = rand() % 100 + 1;
	if (hitRate(enemy[randomNumber]) >= hitRateChance)
	{
		damage = setDamage(enemy[randomNumber], hasBonusDamage(enemy[randomNumber]), coEff);
		damage = damageCrit(damage);
		cout << className << " used a basic attack against " << enemy[randomNumber]->getClassName();
		enemy[randomNumber]->setClassHealth(getBaseDamage(coEff) - enemy[randomNumber]->getClassVit());
	}
	else
	{
		cout << className << " missed a basic attack against " << enemy[randomNumber]->getClassName();
	}
}

void Class::attackEnemy(vector<Class*> enemy, bool isBasicAttack)
{
}

void Class::displaySkills()
{
}

bool Class::hasBonusDamage(Class* enemy)
{
	return false;
}

float Class::hitRate(Class* enemy)
{
	float hitRate = (classDex / enemy->classAgi) * 100;
	if (hitRate < 20)
	{
		return 20;
	}
	else if (hitRate > 80)
	{
		return 80;
	}
	else
	{
		return hitRate;
	}
}
