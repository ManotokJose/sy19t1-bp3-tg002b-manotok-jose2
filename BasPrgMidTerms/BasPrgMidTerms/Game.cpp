#include "Game.h"
#include <iostream>
#include <algorithm>

using namespace std;

Game::Game(Player* p)
{
	Player* e = new Player(true);

	player = p;
	enemy = e;
}


Game::~Game()
{
	delete player;
	delete enemy;
}

void Game::gameCombat()
{
	int teamCounter = 0;
	bool isBasicAttack;

	for (int i = 0; i < 3; i++)
	{
		turnOrder.push_back(enemy->playerTeam[i]);
	}
	for (int i = 0; i < 3; i++)
	{
		turnOrder.push_back(player->playerTeam[i]);
	}

	sortTurnOrder();

	cout << endl;

	while (player->isTeamDead() == false && enemy->isTeamDead() == false)
	{
		if (turnOrder[0]->isPlayerUnit())
		{
			int playerInput;
			cout << player->playerTeam[0]->getClassName() << "'s turn" << endl << endl;
			player->playerTeam[0]->displaySkills();
			cin >> playerInput;
			switch (playerInput)
			{
			case 1:
				player->playerTeam[0]->attackEnemy(enemy->playerTeam, true);
				break;
				player->playerTeam[0]->attackEnemy(enemy->playerTeam, false);
			case 2:
				break;
			}
		}
		else
		{
			cout << "enemy turn" << endl;
		}
		system("pause");



		teamCounter++;
	}

	turnOrder.clear();
}

void Game::sortTurnOrder() //code from https://mathbits.com/MathBits/CompSci/Arrays/Bubble.htm
{
	int i;
	int j;
	int loopCount = 1;
	Class* temp;

	for (int i = 1; (i <= turnOrder.size()) && loopCount; i++)
	{
		loopCount = 0;
		for (int j = 0; j < (turnOrder.size() - 1); j++)
		{
			if (turnOrder[j + 1]->getAgi() > turnOrder[j]->getAgi())
			{
				temp = turnOrder[j];
				turnOrder[j] = turnOrder[j + 1];
				turnOrder[j + 1] = temp;
				loopCount = 1;
			}
		}
	}
}


