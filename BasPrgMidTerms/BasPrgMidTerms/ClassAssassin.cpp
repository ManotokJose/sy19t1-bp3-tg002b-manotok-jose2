#include "ClassAssassin.h"
#include <iostream>

using namespace std;

ClassAssassin::ClassAssassin()
{
	className = "Enemy Assassin";

	classType = "Assassin";
	classLevel = 1;
	classPow = 10;
	classHealth = 10;
	classAgi = 3;
	classDex = 15;
	classVit = 5;
}


ClassAssassin::~ClassAssassin()
{
}

void ClassAssassin::setClassLevel()
{
	classLevel++;
}

void ClassAssassin::attackEnemy(vector<Class*> enemy, bool isBasicAttack)
{
	int damage;
	if (isBasicAttack == false)
	{
		Class* target = enemy[0];
		for (int i = 0; i < enemy.size(); i++)
		{
			if (target->getHealth() > enemy[i]->getHealth())
			{
				target = enemy[i];
			}
		}

		damage = setDamage(target, hasBonusDamage(target), 2.2);

		for (int i = 0; i < enemy.size(); i++)
		{
			if (target->getUnitNumber() == enemy[i]->getUnitNumber())
			{
				cout << className << " used a skill against " << enemy[i]->getClassName();
				enemy[i]->setClassHealth(-1 * damage);
			}
		}

		delete target;
	}
	else
	{
		damageEnemy(enemy, 1.0);
	}
}

bool ClassAssassin::hasBonusDamage(Class* enemy)
{
	if (enemy->getClassType() == "Mage")
	{
		return true;
	}
	else
	{
		return false;
	}
}

int ClassAssassin::getClassLevel()
{
	return classLevel;
}

void ClassAssassin::displaySkills()
{
	cout << "[1] Basic Attack" << endl;
	cout << "[2] Assassinate" << endl;
}
