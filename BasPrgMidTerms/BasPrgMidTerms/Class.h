#pragma once
#include <string>
#include <vector>

using namespace std;

class Class
{
public:
	Class();
	~Class();
	
	virtual void attackEnemy(vector<Class*> enemy, bool isBasicAttack);
	virtual void displaySkills();
	virtual bool hasBonusDamage(Class* enemy);

	float hitRate(Class* enemy);

	int getBaseDamage(float coEff);
	int getClassLevel();
	int getHealth();
	int getAgi();
	int getClassDex();
	int getClassVit();
	int getUnitNumber();
	int damageCrit(int number);
	bool isPlayerUnit();
	string getClassName();
	string getClassType();
	void setClassName();
	void setClassHealth(int number);
	void setPlayerUnit(bool isPlayerUnit);
	void setUnitNumber(int number);
	int setDamage(Class* enemy, bool bonusDamage, int coEff);
	void damageEnemy(vector<Class*> enemy, float coEff);

protected:
	int classHealth;

	bool playerUnit;

	string classType;
	string className;
	int classPow;
	int classLevel;
	int classAgi;
	int classDex;
	int classVit;
	int unitNumber;
};

