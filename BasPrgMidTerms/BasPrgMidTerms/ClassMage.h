#pragma once
#include "Class.h"
#include <iostream>

using namespace std;

class ClassMage : public Class
{
public:
	ClassMage();
	~ClassMage();

	void setClassLevel();
	void attackEnemy(vector<Class*> enemy, bool isBasicAttack);
	bool hasBonusDamage(Class* enemy);
	int getClassLevel();
	void displaySkills();
};

