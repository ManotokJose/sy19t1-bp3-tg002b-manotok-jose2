#include <iostream>
#include <string>
#include <time.h>
#include "Game.h"

using namespace std;

int main()
{
	srand(time(NULL));

	Player* player = new Player(false);

	Game* newGame = new Game(player);
	newGame->gameCombat();

	system("pause");
	delete newGame;
}