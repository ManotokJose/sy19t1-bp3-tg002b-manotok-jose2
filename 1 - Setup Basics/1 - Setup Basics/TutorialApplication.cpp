/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	testFunc(0, 0, 0);
	testFunc(10, 0, 0);
	testFunc(-10, 0, 0);
	testFunc(0, 20, 0);
}

void TutorialApplication::testFunc(int x, int y, int z)
{
	ManualObject* object = mSceneMgr->createManualObject();
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//s1
	object->position(-10 + x, 10 + y, 0 + z);
	object->position(-10 + x, 0 + y, 0 + z);
	object->position(0 + x, 0 + y, 0 + z);

	object->position(-10 + x, 10 + y, 0 + z);
	object->position(0 + x, 0 + y, 0 + z);
	object->position(0 + x, 10 + y, 0 + z);

	//s2
	object->position(0 + x, 10 + y, 0 + z);
	object->position(0 + x, 0 + y, 0 + z);
	object->position(0 + x, 0 + y, -10 + z);

	object->position(0 + x, 10 + y, 0 + z);
	object->position(0 + x, 0 + y, -10 + z);
	object->position(0 + x, 10 + y, -10 + z);

	//s3
	object->position(-10 + x, 10 + y, 0 + z);
	object->position(-10 + x, 0 + y, -10 + z);
	object->position(-10 + x, 0 + y, 0 + z);

	object->position(-10 + x, 10 + y, 0 + z);
	object->position(-10 + x, 10 + y, -10 + z);
	object->position(-10 + x, 0 + y, -10 + z);

	//s4
	object->position(-10 + x, 10 + y, -10 + z);
	object->position(0 + x, 0 + y, -10 + z);
	object->position(-10 + x, 0 + y, -10 + z);

	object->position(-10 + x, 10 + y, -10 + z);
	object->position(0 + x, 10 + y, -10 + z);
	object->position(0 + x, 0 + y, -10 + z);

	//s5
	object->position(-10 + x, 10 + y, -10 + z);
	object->position(-10 + x, 10 + y, 0 + z);
	object->position(0 + x, 10 + y, 0 + z);

	object->position(-10 + x, 10 + y, -10 + z);
	object->position(0 + x, 10 + y, 0 + z);
	object->position(0 + x, 10 + y, -10 + z);


	//s6
	object->position(-10 + x, 0 + y, -10 + z);
	object->position(0 + x, 0 + y, 0 + z);
	object->position(-10 + x, 0 + y, 0 + z);

	object->position(-10 + x, 0 + y, -10 + z);
	object->position(0 + x, 0 + y, -10 + z);
	object->position(0 + x, 0 + y, 0 + z);

	object->end();
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
