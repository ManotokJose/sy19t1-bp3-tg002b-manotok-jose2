#include <string>
#include <iostream>
#include <time.h>
#include "Character.h"
#include "Item.h"
#include "HealthPotion.h"

using namespace std;

int main()
{
	srand(time(NULL));
	Character* player = new Character;

	while (player->getPlayerCrystal() > 0 && player->getPlayerHealth() > 0)
	{
		cout << "Health: " << player->getPlayerHealth() << endl;
		cout << "Crystal: " << player->getPlayerCrystal() << endl;
		cout << "Rolled: ";
		player->setPlayerCrystal(-5);
		player->rollItem();
		system("Pause");
	}

	cout << endl;
	player->displayEndStats();

	system("pause");

}