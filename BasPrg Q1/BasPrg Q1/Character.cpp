#include "Character.h"
#include "HealthPotion.h"
#include "SRPoint.h"
#include "RPoints.h"
#include "Bomb.h"
#include "Crystal.h"
#include "SSRPoint.h"


Character::Character()
{
	points = 0;
	health = 100;
	crystal = 100;
	pointsBomb = 0;
	pointsHealth = 0;
	pointsCrystal = 0;
	pointsR = 0;
	pointsSR = 0;
	pointsSSR = 0;
}


Character::~Character()
{
}

void Character::rollItem()
{
	// ssr 1
	// sr 9
	// r 40
	// hp 15
	// isis 20
	// meth 15

	//1 = ssr 2 - 11 = sr 12 - 52 = r 53 - 68 = hp 69 - 89 = isis 90 - 105? = meth

	int rollNumber = randomNumber(105, 1);

	if (rollNumber == 1)
	{
		cout << "SSR" << endl;
		cout << "50 Points" << endl;
		SSRPoint* ssr = new SSRPoint(this);
		delete ssr;
	}
	else if (rollNumber >= 2 && rollNumber <= 11)
	{
		cout << "SR" << endl;
		cout << "10 points" << endl;
		SRPoint* sr = new SRPoint(this);
		delete sr;
	}
	else if (rollNumber >= 12 && rollNumber <= 52)
	{
		cout << "R" << endl;
		cout << "1 point" << endl;
		RPoints* r = new RPoints(this);
	}
	else if (rollNumber >= 53 && rollNumber <= 68)
	{
		cout << "Health Potion" << endl;
		cout << "30 Health Points" << endl;
		HealthPotion* hp = new HealthPotion(this);
		delete hp;
	}
	else if (rollNumber >= 69 && rollNumber <= 89)
	{
		cout << "Bomb" << endl;
		cout << "-25 Health Points" << endl;
		Bomb* bomb = new Bomb(this);
		delete bomb;
	}
	else
	{
		cout << "Crystal" << endl;
		cout << "15 Crystals" << endl;
		Crystal* crystal = new Crystal(this);
		delete crystal;
	}
}

void Character::displayEndStats()
{
	cout << "Health Potion: " << pointsHealth << endl;
	cout << "Bomb: " << pointsBomb << endl;
	cout << "Crystal: " << pointsCrystal << endl;
	cout << "R: " << pointsR << endl;
	cout << "SR: " << pointsSR << endl;
	cout << "SSR: " << pointsSSR << endl;
	cout << "Total points: " << points;

}

int Character::getPlayerHealth()
{
	return health;
}

int Character::getPlayerCrystal()
{
	return crystal;
}

int Character::getPlayerPoints()
{
	return points;
}

void Character::setPlayerPoints(int number)
{
	points += number;
}

void Character::setPlayerHealth(int number)
{
	health += number;
}

void Character::setPlayerCrystal(int number)
{
	crystal += number;
}

void Character::setHealthPoints()
{
	pointsHealth++;
}

void Character::setBombPoints()
{
	pointsBomb++;
}

void Character::setCrystalPoints()
{
	pointsCrystal++;
}

void Character::setRPoints()
{
	pointsR++;
}

void Character::setSRPoints()
{
	pointsSR++;
}

void Character::setSSRPoints()
{
	pointsSSR++;
}

int Character::randomNumber(int max, int min)
{
	return rand() % max + min;
}

bool Character::purchaseTransaction()
{
	return true;
}
