#pragma once
#include "Item.h"
class Character;
class Bomb : public Item
{
public:
	Bomb(Character* player);
	~Bomb();
};

