#include "Character.h"



Character::Character()
{
	health = 100;
	mp = 100;
	maxMp = 100;
	characterWeapon = new Weapon();
}


Character::~Character()
{
}

int Character::getDamage()
{
	int damageApplied = characterWeapon->weaponDamage;
	int temp = 0;

	mp += characterManaRegen();

	for (int i = 0; i < 1; i++)
	{
		damageApplied += temp;
		switch (randomNumber())
		{
		case 1:
			if (mpCheck(20))
			{
				cout << "Normal Attack!" << endl;
				mp -= 20;
				cout << damageApplied << " Damage dealt" << endl;
				return damageApplied;
			}
			else
			{
				cout << "Not enough mana!" << endl;
				i--;
			}
			break;
		case 2:
			if (mpCheck(20))
			{
				cout << "Critical Attack!" << endl;
				mp -= 20;
				cout << damageApplied << " Damage dealt" << endl;
				damageApplied *= 2;
				return damageApplied;
			}
			else
			{
				cout << "Not enough mana!" << endl;
				i--;
			}
			break;
		case 3:
			if (mpCheck(40))
			{
				cout << "Syphon Attack!" << endl;
				mp -= 40;
				health += damageApplied * .25;
				cout << damageApplied << " Damage dealt" << endl;
				return damageApplied;
			}
			else
			{
				cout << "Not enough mana!" << endl;
				i--;
			}
			break;
		case 4:
			if (mpCheck(50))
			{
				cout << "Vengance Attack!" << endl;
				if (health > 1)
				{
					health -= health * .25;
				}
				mp -= 50;
				damageApplied += damageApplied * 2;
				cout << damageApplied << " Damage dealt" << endl;
				return damageApplied;
			}
			else
			{
				cout << "Not enough mana!" << endl;
				i--;
			}
			break;
		case 5:
			if (mpCheck(50))
			{
				cout << "Dopel Blade!" << endl;
				i--;
				mp -= 50;
				temp = damageApplied;
				break;
			}
			else
			{
				cout << "Not enough mana!" << endl;
				i--;
			}
		}
	}
}

void Character::attackCharacter(Character* enemy)
{
	enemy->health -= this->getDamage();
}

bool Character::mpCheck(int cmp)
{
	if (cmp <= mp)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Character::randomNumber()
{
	return rand() % 5 + 1;
}

int Character::characterManaRegen()
{
	return maxMp * .25;
}
