#include <string>
#include <iostream>
#include <time.h>
#include "Character.h"

using namespace std;

int main()
{
	srand(time(NULL));

	Character* player = new Character;
	Character* enemy = new Character;

	while (player->health > 0 && enemy->health > 0)
	{
		cout << "Player health: " << player->health << endl;
		cout << "Enemy health: " << enemy->health << endl;
		
		cout << "Player attacks the enemy!" << endl;
		player->attackCharacter(enemy);
		
		system("pause");

		cout << "Enemy attacks the player!" << endl;
		enemy->attackCharacter(player);

		system("pause");
	}

	if (player->health > 0)
	{
		cout << "Player wins with " << player->health << " HP left!";
	}
	else
	{
		cout << "Enemy wins with " << enemy->health << " HP left!";
	}
	cout << endl;
	system("pause");
}