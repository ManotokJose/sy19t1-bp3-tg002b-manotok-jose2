/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
#include <string>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	speed = 1;
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	ManualObject* object = mSceneMgr->createManualObject();
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//s1
	object->position(-10, 10, 0);
	object->colour(0, 0, 1);
	object->position(-10, 0, 0);
	object->colour(0, 1, 0);
	object->position(0, 0, 0);
	object->colour(1, 0, 0);

	object->position(-10, 10, 0);
	object->colour(0, 0, 1);
	object->position(0, 0, 0);
	object->colour(0, 1, 0);
	object->position(0, 10, 0);
	object->colour(1, 0, 0);

	//s2
	object->position(0, 10, 0);
	object->position(0, 0, 0);
	object->position(0, 0, -10);

	object->position(0, 10, 0);
	object->position(0, 0, -10);
	object->position(0, 10, -10);

	//s3
	object->position(-10, 10, 0);
	object->position(-10, 0, -10);
	object->position(-10, 0, 0);

	object->position(-10, 10, 0);
	object->position(-10, 10, -10);
	object->position(-10, 0, -10);

	//s4
	object->position(-10, 10, -10);
	object->position(0, 0, -10);
	object->position(-10, 0, -10);

	object->position(-10, 10, -10);
	object->position(0, 10, -10);
	object->position(0, 0, -10);

	//s5
	object->position(-10, 10, -10);
	object->position(-10, 10, 0);
	object->position(0, 10, 0);

	object->position(-10, 10, -10);
	object->position(0, 10, 0);
	object->position(0, 10, -10);


	//s6
	object->position(-10, 0, -10);
	object->position(0, 0, 0);
	object->position(-10, 0, 0);

	object->position(-10, 0, -10);
	object->position(0, 0, -10);
	object->position(0, 0, 0);

	object->end();

	cubeObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeObject->attachObject(object);
}

bool TutorialApplication::isMovingUp()
{
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

bool TutorialApplication::isMovingDown()
{
	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool TutorialApplication::isMovingLeft()
{
	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool TutorialApplication::isMovingRight()
{
	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	if (isMovingUp())
	{
		cubeObject->translate(0, speed, 0);
	}
	
	if (isMovingRight())
	{
		cubeObject->translate(speed, 0, 0);
	}

	if (isMovingDown())
	{
		cubeObject->translate(0, (-1 * speed), 0);
	}

	if (isMovingLeft())
	{
		cubeObject->translate((-1 * speed), 0, 0);
	}

	if (isMovingUp() == false && isMovingDown() == false && isMovingLeft() == false && isMovingRight() == false)
	{
		if (speed > 0)
		{
			speed -= 2 * evt.timeSinceLastFrame;
		}

		if (speed < 0)
		{
			speed = 0;
		}
	}
	else
	{
		speed += 1 * evt.timeSinceLastFrame;
	}
	return true;
}


//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception & e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
