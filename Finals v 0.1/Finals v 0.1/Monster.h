#pragma once
#include "Armour.h"
#include "Weapon.h"
#include "Shop.h"
#include <string>

using namespace std;

class Monster
{
public:
	Monster(int playerLevel);
	~Monster();

	string monsterName;

	int monsterLevel;
	int monsterHealth;
	int monsterMaxHealth;
	int monsterAttack; //POW
	int monsterDefense; //VIT
	int monsterAgility; //D
	int monsterDexterity; //H
	int monsterExp;

	Weapon* monsterOffense;
	Weapon* monsterOffhand;

	Armour* monsterHelmet;
	Armour* monsterChestplate;
	Armour* monsterGauntlet;
	Armour* monsterLegArmour;

	int getDamage();
	int getArmour();

private:
	int randomNumber(int max, int min);
	void setValues(string name, int health, int atk, int def, int agi, int dex, int exp);
};

