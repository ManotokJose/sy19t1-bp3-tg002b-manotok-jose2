#include "Monster.h"



Monster::Monster(int playerLevel)
{
	string name[8] = { "Orc", "Goblin", "Kobold", "Owlbear", "Aboleth", "Beholder", "Imp" };
	int monsterNumber = randomNumber(8, 1);

	monsterLevel = playerLevel;

	monsterOffense = new Weapon(false);
	monsterOffhand = new Weapon(true);

	monsterHelmet = new Armour(1);
	monsterChestplate = new Armour(2);
	monsterGauntlet = new Armour(3);
	monsterLegArmour = new Armour(4);

	switch (monsterNumber)
	{
	case 1: //Orc HHealth, MAtk, MDef
		setValues("Orc", 9, 6, 7, 5, 5, 100);
		break;

	case 2: //Goblin LHealth, MAtk, MDef
		setValues("Goblin", 4, 6, 5, 6, 6, 20);
		break;

	case 3: //Kobold MHealthm,MAtk, MDef
		setValues("Kobold", 5, 7, 5, 3, 3, 50);
		break;

	case 4: //Owlbear HDef, HAtk, LHealth
		setValues("Owlbear", 4, 9, 9, 7, 6, 400);
		break;

	case 5: //Aboleth HDodge, MHit, MAtk
		setValues("Aboleth", 4, 7, 5, 8, 7, 600);
		break;

	case 6: //Beholder HAtk, MHit, MDef
		setValues("Beholder", 6, 9, 5, 5, 7, 700);
		break;

	case 7: //Imp HAgi, HAtk, LHealth
		setValues("Imp", 2, 9, 2, 7, 6, 10);
		break;

	case 8: //Nargacuga HAtk, HAgi, MHealth
		setValues("Nargacuga", 8, 9, 6, 9, 9, 1000);
		break;
	}

	if (randomNumber(100,1) == 1 ) //Big boi boos
	{
		setValues("Orc Lord", 10, 10, 10, 10, 10, 10000);
	}
}

Monster::~Monster()
{
}

int Monster::getDamage()
{
	return monsterAttack + monsterOffense->weaponDamage + monsterOffhand->weaponDamage;
}

int Monster::getArmour()
{
	return monsterDefense + monsterHelmet->armourDamage + monsterChestplate->armourDamage + monsterGauntlet->armourDamage + monsterLegArmour->armourDamage + monsterOffhand->damageNegation;
}

int Monster::randomNumber(int max, int min)
{
	return rand() % (max + min);
}

void Monster::setValues(string name, int health,  int atk, int def, int agi, int dex, int exp)
{
	monsterName = name;
	monsterHealth = health + monsterLevel;
	monsterMaxHealth = health + monsterLevel;
	monsterAttack = atk + monsterLevel;
	monsterDefense = def + monsterLevel;
	monsterAgility = agi + monsterLevel;
	monsterDexterity = dex + monsterLevel;
	monsterExp = exp * monsterLevel;
}
