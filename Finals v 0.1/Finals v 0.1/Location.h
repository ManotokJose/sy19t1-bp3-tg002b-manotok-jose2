#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <conio.h>
#include "Shop.h"
#include "Monster.h"
#include "Player.h"


using namespace std;

class Location
{
public:
	Location(int x, int y);
	~Location();

	int locationType; // 1 nothing, 2 monster, 3 shop

	int locationX;
	int locationY;

	bool hasBeenSeen;

	Player* locationPlayer;
	Shop* locationShop;
	Monster* locationMonster;
	
	void displayLocation();
	void locationAmbush();

private: 

	int randomNumber(int max, int min);
};
 