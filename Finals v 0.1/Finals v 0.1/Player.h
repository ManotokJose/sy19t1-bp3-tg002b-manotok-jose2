#pragma once
#include <string>
#include <vector>
#include "Armour.h"
#include "Weapon.h"

using namespace std;

class Player
{
public:
	Player();
	~Player();

	string playerName;
	string playerClass;

	int playerLevel;
	int playerHealth;
	int playerMaxHealth;
	int playerAttack; //POW
	int playerDefense; //VIT
	int playerAgility; //Dodge
	int playerDexterity; //Hit
	int playerLuck;
	int playerGold;
	int playerExp;
	int reqExp;

	int playerPosX;
	int playerPosY;

	bool isTwoHanding;
	bool inCombat;

	Weapon* playerOffense;
	Weapon* playerOffhand;

	Armour* playerHelmet;
	Armour* playerChestplate;
	Armour* playerGauntlet;
	Armour* playerLegArmour;

	void setValues(string name, int health, int atk, int def, int agi, int dex, string pClass);
	int getDamage();
	int getArmour();

	void displayStats();
	void playerRest();
	void levelUp();

private:
	int randomNumber(int max, int min);
};

