#include "Weapon.h"



Weapon::Weapon(bool offHand)
{
	string prefixList[11] = { "Dagger", "Short Sword", "Long Sword", "Mace", "Hammer", "Great Hammer", "Great Sword", "Bow", "Spear", "Katana", "Laser Gun"};
	string prefixOffhand[2] = { "Shield", "Great Shield" };
	string elementList[4] = { "Light", "Dark", "", "" };
	string specialElement[5] = { "Acidic", "Flaming", "Magic", "Electric", "" };

	if (offHand == true)
	{
		if (randomNumber(100, 1) >= 25)
		{
			weaponType = prefixList[randomNumber(2,0)];
		}
		else
		{
			weaponType = prefixList[randomNumber(11, 0)];
		}
	}
	else
	{
		weaponType = prefixList[randomNumber(11, 0)];
	}

	if (randomNumber(100, 1) == 1)
	{
		weaponType = "Laser Gun";
	}

	weaponElement = elementList[randomNumber(4, 0)];
	weaponSpecialElement = specialElement[randomNumber(5, 0)];

	weaponName = weaponElement + " " + weaponSpecialElement + " " + weaponType;

	if (weaponType == "Dagger")
	{
		setValues(false, 15, 5, 70);
	}

	if (weaponType == "Short Sword")
	{
		setValues(false, 20, 10, 30);
	}

	if (weaponType == "Long Sword")
	{
		setValues(false, 30, 15, 20);
	}

	if (weaponType == "Mace")
	{
		setValues(false, 40, 5, 15);
	}

	if (weaponType == "Hammer")
	{
		setValues(false, 45, 10, 10);
	}

	if (weaponType == "Great Hammer")
	{
		setValues(true, 60, 20, 10);
	}

	if (weaponType == "Great Sword")
	{
		setValues(true, 75, 30, 5);
	}

	if (weaponType == "Bow")
	{
		setValues(true, 50, 0, 40);
	}

	if (weaponType == "Spear")
	{
		setValues(true, 30, 0, 50);
	}

	if (weaponType == "Katana")
	{
		setValues(true, 60, 50, 60);
	}

	if (weaponType == "Laser Gun")
	{
		setValues(true, 100, 100, 100);
	}

	if (weaponType == "Shield")
	{
		setValues(false, 10, 45, 0);
	}

	if (weaponType == "Great Shield")
	{
		setValues(true, 30, 80, 0);
	}
}

Weapon::~Weapon()
{
}

int Weapon::randomNumber(int max, int min)
{
	return rand() % max + min;
}

void Weapon::setValues(bool twoHanded, int damage, int damNeg, int crit)
{
	onlyTwoHanded = twoHanded;
	weaponDamage = damage;
	//armourPen = pen;
	damageNegation = damNeg;
	//critChance = crit;
}

