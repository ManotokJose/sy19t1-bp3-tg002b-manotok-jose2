#include "Shop.h"


Shop::Shop()
{
	for (int i = 0; i < randomNumber(5,1); i++)
	{
		shopWeapon.push_back(new Weapon(randomNumber(1, 0)));
	}

	for (int i = 0; i < randomNumber(5,1); i++)
	{
		shopArmour.push_back(new Armour(randomNumber(4, 1)));
	}
}


Shop::~Shop()
{
}

void Shop::displayWares()
{
	int playerInput;
	cout << "Weapons: " << endl;
	for (int i = 0; i < shopWeapon.size(); i++)
	{
		shopWeapon[i]->weaponPrice = shopCustomer->playerLevel * 1.5;
		cout << "[" << i << "]" << shopWeapon[i] << endl;
	}
	cout << "Item Number (if nothing suits your interest, type anyother number): ";
	cin >> playerInput;
	if (playerInput < shopWeapon.size())
	{
		int buyAnswer;
		cout << "You picked " << shopWeapon[playerInput - 1]->weaponName << " for " << shopWeapon[playerInput - 1]->weaponPrice << " Gold" << endl;
		cout << "Would you like to buy it?" << endl;
		cout << "[1] Yes" << endl;
		cout << "[anything else] No" << endl;
		cin >> buyAnswer;
		if (buyAnswer == 1)
		{
			int handAnswer;
			cout << "Which hand would you equip it on?" << endl;
			cout << "[1] Main" << endl;
			cout << "[2] Offhand" << endl;
			cin >> handAnswer;
			if (handAnswer == 1) // 3 ifs... yikes
			{
				shopCustomer->playerGold += shopCustomer->playerOffense->weaponPrice / 2;
				shopCustomer->playerOffense->~Weapon();
				shopCustomer->playerOffense = shopWeapon[playerInput - 1];
				shopCustomer->playerGold -= shopWeapon[playerInput - 1]->weaponPrice;
			}
			else
			{
				shopCustomer->playerGold += shopCustomer->playerOffhand->weaponPrice / 2;
				shopCustomer->playerOffhand->~Weapon();
				shopCustomer->playerOffhand = shopWeapon[playerInput - 1];
				shopCustomer->playerGold -= shopWeapon[playerInput - 1]->weaponPrice;
			}
		}
	}
	cout << "Armour: " << endl;
	for (int i = 0; i < shopArmour.size(); i++)
	{
		shopArmour[i]->armourPrice = shopCustomer->playerLevel * 1.5;
		cout << "[" << i << "]" << shopArmour[i] << endl;
	}
	cout << "Item Number (if nothing suits your interest, type anyother number): ";
	cin >> playerInput;
	if (playerInput < shopArmour.size()) //how else am i supposed to do this?
	{
		int buyAnswer;
		cout << "You picked " << shopArmour[playerInput - 1]->armourName << " for " << shopArmour[playerInput - 1]->armourPrice << " Gold" << endl;
		cout << "Would you like to buy it?" << endl;
		cout << "[1] Yes" << endl;
		cout << "[anything else] No" << endl;
		cin >> buyAnswer;
		if (buyAnswer == 1)
		{
			if (shopArmour[playerInput]->armourType == "Helmet")
			{
				shopCustomer->playerGold += shopArmour[playerInput - 1]->armourPrice / 2;
				shopCustomer->playerHelmet->~Armour();
				shopCustomer->playerHelmet = shopArmour[playerInput - 1];
				shopCustomer->playerGold -= shopArmour[playerInput - 1]->armourPrice;
			}

			if (shopArmour[playerInput]->armourType == "Chestplate")
			{
				shopCustomer->playerGold += shopArmour[playerInput - 1]->armourPrice / 2;
				shopCustomer->playerChestplate->~Armour();
				shopCustomer->playerChestplate = shopArmour[playerInput - 1];
				shopCustomer->playerGold -= shopArmour[playerInput - 1]->armourPrice;
			}

			if (shopArmour[playerInput]->armourType == "Gauntlet")
			{
				shopCustomer->playerGold += shopArmour[playerInput - 1]->armourPrice / 2;
				shopCustomer->playerGauntlet->~Armour();
				shopCustomer->playerGauntlet = shopArmour[playerInput - 1];
				shopCustomer->playerGold -= shopArmour[playerInput - 1]->armourPrice;
			}

			if (shopArmour[playerInput]->armourType == "Leg Armour")
			{
				shopCustomer->playerGold += shopArmour[playerInput - 1]->armourPrice / 2;
				shopCustomer->playerLegArmour->~Armour();
				shopCustomer->playerLegArmour = shopArmour[playerInput - 1];
				shopCustomer->playerGold -= shopArmour[playerInput - 1]->armourPrice;
			}
		}
	}
}

bool Shop::checkForMoney()
{
	if (shopCustomer->playerGold > shopCustomer->playerLevel * 1.5)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Shop::randomNumber(int max, int min)
{
	return rand() % max + min;
}
