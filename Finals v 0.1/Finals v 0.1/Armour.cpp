#include "Armour.h"



Armour::Armour(int slot)
{
	string prefix[7] = { "Orcish", "Leather", "Adamanitum", "Underwear" , "Iron", "Plate", "Chain" };

	switch (slot)
	{
	case 1:
		armourType = "Helmet";
		break;

	case 2:
		armourType = "Chestplate";
		break;

	case 3:
		armourType = "Gauntlet";
		break;

	case 4:
		armourType = "Leg Armour";
		break;
	}
	armourPrefix = prefix[randomNumber(7, 0)];
	armourName = armourPrefix + " " + armourType;

	if (armourPrefix == "Orcish")
	{
		armourDamage = 5;
	}

	if (armourPrefix == "Leather")
	{
		armourDamage = 3;
	}

	if (armourPrefix == "Adamantium")
	{
		armourDamage = 9;
	}

	if (armourPrefix == "Underwear")
	{
		armourDamage = 1;
	}

	if (armourPrefix == "Iron")
	{
		armourDamage = 7;
	}

	if (armourPrefix == "Plate")
	{
		armourDamage = 6;
	}

	if (armourPrefix == "Chain");
	{
		armourDamage = 4;
	}
}


Armour::~Armour()
{
}

int Armour::randomNumber(int max, int min)
{
	return rand() % max + min;
}

