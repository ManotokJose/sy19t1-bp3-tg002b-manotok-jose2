#pragma once
#include "Location.h"
#include "Player.h"
class Map
{
public:
	Map();
	~Map();

	int areaCount = 0;
	int currentLocationID;

	vector<Location*> area;
	Player* mapPlayer;

	void gameStart();
	void generateNewArea();
	void areaMove();
	void areaEvent();
	void enterShop();
	void playerMonsterFight();
	bool checkPlayerLevel();

	bool checkArea();

	void monsterAreaFight();
	void playerAreaFight();

private:
	int randomNumber(int max, int min);
};

