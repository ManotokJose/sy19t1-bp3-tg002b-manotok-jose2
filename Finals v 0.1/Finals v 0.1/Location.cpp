#include "Location.h"
#include <iostream>

using namespace std;

Location::Location(int x, int y)
{
	int symbolChance = randomNumber(100, 1);

	if (symbolChance < 60)
	{
		locationType = 1; //clear
	}
	else if (symbolChance > 60 && symbolChance < 90)
	{
		locationType = 2; //enemy
	}
	else
	{
		locationType = 3; //shop
	}

	locationX = x;
	locationY = y;

	if (locationX == 0 && locationY == 0)
	{
		cout << "Move with WASD" << endl;
		cout << "who moves with the the arrow keys these days?" << endl;
		cout << "You start with random weapons and armour" << endl;
		locationType = 1;
	}
}


Location::~Location()
{
}

void Location::displayLocation()
{
	cout << "(x,y): " << locationX << " " << locationY << endl;
	if (locationType == 3)
	{
		cout << "there is a shop here, you may need to press enter to trigger the event" << endl;
	}
}

void Location::locationAmbush()
{
	if ((randomNumber(100, 1) <= 5) && locationType != 3)
	{
		locationType = 2;
		cout << "You got ambushed!" << endl;
	}
}

int Location::randomNumber(int max, int min)
{
	return rand() % max +  min;
}


